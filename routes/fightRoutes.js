const {Router} = require('express');
const FightService = require('../services/fightService');
const {responseMiddleware} = require('../middlewares/response.middleware');


const router = Router();

router.get('/', (req, res, next) => {
    try {
        const fightsLogs = FightService.getAllLogs();
        res.data = fightsLogs;
    } catch (error) {
        res.err = error;
    } finally {
        next()
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const fightLog = FightService.getLogById(req.params.id);
        res.data = fightLog;
    } catch (error) {
        res.err = error;
    } finally {
        next()
    }
}, responseMiddleware);

router.post('/', (req, res, next) => {
    try {
        const logs = FightService.saveLog(req.body);
        res.data = logs
    } catch (error) {
        res.err = error
    } finally {
        next();
    }
}, responseMiddleware);


router.delete('/:id', (req, res, next) => {
    try {
        const deletedLog = FightService.delete(req.params.id);
        res.data = deletedLog;
    } catch (error) {
        res.err = error;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
