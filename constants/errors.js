const errorCode = {
    notFound: 404,
    queryError: 400,
}

exports.errorCode = errorCode;
