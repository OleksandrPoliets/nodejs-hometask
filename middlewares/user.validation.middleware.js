const {user} = require('../models/user');
const {usersFildFalidations} = require('../validations/fildsValidations');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    try {
        const data = usersFildFalidations(req.body, user, UserService);
        req.body = data;
    } catch (e) {
        res.err = e;
    } finally {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    try {
        const data = usersFildFalidations(req.body, user, UserService);
        req.body = data;
    } catch (e) {
        res.err = e;
    } finally {
        next();
    }
    next();
}


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
