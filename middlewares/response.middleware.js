const {errorCode} = require('../constants/errors')
const responseMiddleware = (req, res, next) => {
    if (res.data) {
        return res.status("200").json(res.data);
    }
    if (res.err) {
        return res.status(errorCode[res.err.name])
            .json({
                error: true,
                message: res.err.message,
            });
    }
    next();
}

exports.responseMiddleware = responseMiddleware;
