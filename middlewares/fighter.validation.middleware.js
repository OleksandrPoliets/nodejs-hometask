const {fighter} = require('../models/fighter');
const {fighterFildsValidations} = require('../validations/fildsValidations');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    try {
        const data = fighterFildsValidations(req.body, fighter, FighterService);
        req.body = data;
    } catch (e) {
        res.err = e;
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    try {
        const data = fighterFildsValidations(req.body, fighter, FighterService);
        req.body = data;
    } catch (e) {
        res.err = e;
    } finally {
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
