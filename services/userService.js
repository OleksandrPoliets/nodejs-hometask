const { UserRepository } = require('../repositories/userRepository');

const error = new Error('User not found');
error.name = 'notFound';

class UserService {

    create(data) {
        const item = UserRepository.create(data);
        delete item.id;

        return item;
    }

    update(id, data) {
        const user = UserRepository.update(id, data);
        if (!user.id) {
            throw error;
        }
        return user;
    }

    getUsers() {
        const users = UserRepository.getAll();
        if (!users.length) {
            const error = new Error('Users not found');
            error.name = 'notFound';
            throw error;
        }
        return users;
    }

    get(id) {
        const user = UserRepository.getOne({ id });
        if (!user) {
            throw error;
        }
        return user;
    }

    delete(id) {
        const user = UserRepository.delete(id);
        if (!user.length) {
            throw error;
        }
        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();
