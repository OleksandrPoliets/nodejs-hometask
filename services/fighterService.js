const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {
    constructor() {
        this.error = new Error('Fighter not found');
        this.error.name = 'notFound';
    }

    create(data) {
        const item = FighterRepository.create(data);
        delete item.id;
        return item;
    }

    update(id, data) {
        const fighter = FighterRepository.update(id, data);
        if (!fighter.id) {
            throw this.error;
        }
        return fighter;
    }

    getFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters.length) {
            const error = new Error('Users not found');
            error.name = 'notFound';
            throw this.error;
        }
        return fighters;
    }

    get(id) {
        const fighter = FighterRepository.getOne({id});
        if (!fighter) {
            throw this.error;
        }
        return fighter;
    }

    delete(id) {
        const fighter = FighterRepository.delete(id);
        if (!fighter.length) {
            throw this.error;
        }
        return fighter;
    }

    search(search) {
        const fighter = FighterRepository.getOne(search);
        if (!fighter) {
            return null;
        }
        return fighter;
    }
}

module.exports = new FighterService();
