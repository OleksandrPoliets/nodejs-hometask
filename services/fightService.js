const {FightRepository} = require('../repositories/fightRepository');

class FightersService {
    constructor() {
        this.error = new Error();
    }

    getAllLogs() {
        const logs = FightRepository.getAll();
        if (!logs.length) {
            this.error.message = 'Log not found';
            this.error.name = 'notFound';
            throw this.error;
        }
        return logs;
    }

    getLogById(id) {
        const log = FightRepository.getOne({id});
        if (!log) {
            this.error.message = 'Log not found';
            this.error.name = 'notFound';
            throw this.error;
        }
        return log;
    }

    saveLog(data) {
        const log = FightRepository.create(data);
        if (!log) {
            this.error.message = 'writting Error';
            this.error.name = 'notFound';
            throw this.error;
        }
        return log;
    }

    delete(id) {
        const item = FightRepository.delete(id);
        if (!item) {
            this.error.message = 'Log not found';
            this.error.name = 'notFound';
            throw this.error;
        }
        return item;
    }
}

module.exports = new FightersService();
