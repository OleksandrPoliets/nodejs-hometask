const UserService = require('./userService');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if(!user) {
            const tempUser = UserService.search({email: userData.email});
            if(tempUser){
                const error = new Error('Wrong credentials');
                error.name = 'queryError';

                throw error;
            }else{
                const error = new Error('User not found');
                error.name = 'notFound';

                throw error;
            }
        }
        return user;
    }
}

module.exports = new AuthService();
