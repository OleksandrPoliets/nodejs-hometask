import React, {useState, useRef, useEffect} from 'react';

import './arena.css';
import ArenaWrap from './ArenaComponents/arenaWrap';
import ShowWinner from './showWinner';
import {controls} from './constants/controls';
import {sendLog} from '../../services/domainRequest/fightRequest';
import {getDamage} from './fightHelpers/fight';

const Arena = ({firstFighter, secondFighter}) => {
    const waitTime = 10000;
    const keyCombination = new Set();
    const [firsCriti, setFirstCrit] = useState(new Date() - waitTime);
    const [secondBCrit, setSecondCrit] = useState(new Date() - waitTime);
    const [firsBlock, setFirstBlock] = useState(false);
    const [secondBlock, setSecondBlock] = useState(false);
    const [firstHealth, setHealthFirst] = useState(100);
    const [secondtHealth, setHealthSecond] = useState(100);
    const [winner, setWinner] = useState(null);

    const [fightLog, setFightLog] = useState({
        fighter1: firstFighter.id,
        fighter2: secondFighter.id,
        log: ['Fight started']
    });

    const sendLogs = (fighter) => {
        setFightLog({
            ...fightLog,
            log: fightLog.log.push(`${fighter.name} has won`)
        })
        sendLog(fightLog)
        return true;
    }

    const writeLog = (atacker, defender) => {
        setFightLog({
            ...fightLog,
            log: [
                ...fightLog.log,
                `${atacker.name} damage, ${defender.name} - ${defender.health}%`]
        })
    }

    const getWinner = (winner) => {
        setWinner(winner);
        return true;
    }


    const makeHit = (atacker, defender, damage, setHelth, block1, block2) => {
        if (!block1 && !block2) {
            defender.health -= damage;
            setHelth(defender.health);
            writeLog(atacker, defender);
            if (defender.health <= 0) {
                sendLogs(atacker);
                getWinner(atacker);
            }
        }
    }

    const criticalHit = (atacker, defender, hitTime, sethitTime, helthBar) => {
        const time = new Date();
        if (time - hitTime > waitTime) {
            defender.health -= atacker.power * 2;
            helthBar(defender.health);
            sethitTime(time);
            writeLog(atacker, defender);
            if (defender.health <= 0) {
                sendLogs(atacker);
                getWinner(atacker);
            }
        }
    }

    const onKeyDown = event => {
        switch (event.key) {
            case controls.PlayerOneAttack:
                if (!event.repeat) {
                    makeHit(firstFighter, secondFighter, getDamage(firstFighter, secondFighter), setHealthSecond, firsBlock, secondBlock);
                }
                break;
            case controls.PlayerOneBlock:
                setFirstBlock(true);
                break;

            case controls.PlayerTwoAttack:
                if (!event.repeat) {
                    makeHit(secondFighter, firstFighter, getDamage(secondFighter, firstFighter), setHealthFirst, firsBlock, secondBlock);
                }
                break;

            case controls.PlayerTwoBlock:
                setSecondBlock(true);
                break;
            default:
                keyCombination.add(event.key);

                if (keyCombination.size > 2) {
                    const playerOneCrit = controls.PlayerOneCriticalHitCombination.every(el => keyCombination.has(el));
                    const playerTwoCrit = controls.PlayerTwoCriticalHitCombination.every(el => keyCombination.has(el));

                    if (playerOneCrit) {
                        criticalHit(firstFighter, secondFighter, firsCriti, setFirstCrit, setHealthSecond);
                    }
                    if (playerTwoCrit) {
                        criticalHit(secondFighter, firstFighter, secondBCrit, setSecondCrit, setHealthFirst);
                    }
                }
                break;
        }
    }

    const keyUp = event => {
        keyCombination.clear();
        switch (event.key) {
            case controls.PlayerOneBlock:
                setFirstBlock(false);
                break;

            case controls.PlayerTwoBlock:
                setSecondBlock(false);
                break;
        }
    }


    return (<>
        {winner && <ShowWinner winner={winner}/>}
        <ArenaWrap
            onKeyUp={keyUp}
            onKeyDown={onKeyDown}
            fighter1Helth={firstHealth}
            fighter2Helth={secondtHealth}
            fighter1Name={firstFighter.name}
            fighter2Name={secondFighter.name}
        />
    </>)
}

export default Arena;
