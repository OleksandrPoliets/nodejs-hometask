import React from 'react'
import './showWinner.css';

const showWinner = ({ winner }) => {
    const onClose = () => window.location.reload()
    return (
        <div className='modal-layer'>
            <div className='modal-root'>
                <div className='modal-header'>
                        {`${winner.name} you rock`}
                    <div className='close-btn' onClick={() => onClose()}> × </div>
                </div>
                <div className='modal-body'>
                    Close To Reload
                </div>
            </div>
        </div>
    )
}

export default showWinner
