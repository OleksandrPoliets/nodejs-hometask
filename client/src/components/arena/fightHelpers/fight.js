export function getDamage(attacker, defender) {
    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(defender);

    return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
    const { power } = fighter;
    const criticalHitPower = Math.random() + 1;

    return power * criticalHitPower;
}

export function getBlockPower(fighter) {
    const { defense  } = fighter;
    const blockPower = Math.random() + 1;

    return defense * blockPower;
}
