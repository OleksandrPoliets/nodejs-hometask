import React from "react";

const FighterImage = props => {
    return(
        <div className="fighter-preview___root fighter-preview___left">
            <img
                src={props.imgSorce}
                alt={props.alt}
                title={props.title}
                className="fighter-preview___img"
            />
        </div>
    );
}

export default FighterImage;
