import React from "react";
import image from '../img/versus.png';
import HeltBar from "./helthBar";
import FighterImage from "./fighterImg";
import '../arena.css'
const ArenaWrap = ({fighter1Helth, fighter2Helth, fighter1Name, fighter2Name, onKeyDown, onKeyUp, showDamage}) => {
    return (
        <div
            className="arena___root" tabIndex="0"
            onKeyDown={(e) => onKeyDown(e)}
            onKeyUp={(e) => onKeyUp(e)}
        >
            <div className="arena___fight-status">
                <HeltBar
                    fighterName = {fighter1Name}
                    fighterHelth = {fighter1Helth}
                    fighterIdHeltBar='left-fighter-indicator'
                />

                <div className="arena___versus-sign">
                    <img src={image} alt="vs"/>
                    <div className="arena___damageBlock" ref={showDamage}></div>
                </div>

                <HeltBar
                    fighterName = {fighter2Name}
                    fighterHelth = {fighter2Helth}
                    fighterIdHeltBar='right-fighter-indicator'
                />

            </div>

            <div className="arena___battlefield">

                <FighterImage
                    title={fighter1Name}
                    imgSorce='https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif'
                    alt='fighter1'

                />
                <FighterImage
                    title={fighter2Name}
                    imgSorce='https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif'
                    alt='fighter2'

                />

            </div>
        </div>
    );
};

export default ArenaWrap;
