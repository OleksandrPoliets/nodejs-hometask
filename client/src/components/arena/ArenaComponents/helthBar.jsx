import React from "react";

const HeltBar = props => {
    const helthBarWidth = {
        width: `${props.fighterHelth}%`
    }
    return(
        <div className="arena___fighter-indicator">
            <span className="arena___fighter-name">{props.fighterName}</span>
            <div className="arena___health-indicator">
                <div
                    style={helthBarWidth}
                    className="arena___health-bar"
                    id={props.fighterIdHeltBar}
                ></div>
            </div>
        </div>
    );
}

export default HeltBar;
