const usersFildFalidations = (userData, userSchema, services) => {
    const {email = '', password = '', firstName = '', lastName = '', phoneNumber = ''} = userData;
    const user = {...userSchema};
    delete user.id;

    switch (false) {
        case checkReqData(userData, user):
            throw createResponseError('queryError', 'All fields requerid');

        case !services.search({email}):
            throw createResponseError('queryError', 'The email is already registered. Please sign in.');

        case !services.search({phoneNumber}):
            throw createResponseError('queryError', 'Phone number is already registered.');

        case validateName(firstName):
            throw createResponseError('queryError', 'Wrong first Name only letters and length more then 0');

        case validateName(lastName):
            throw  createResponseError('queryError', 'Wrong last Name only letters and length more then 0');

        case validateEmail(email):
            throw createResponseError('queryError', 'Wrong email format is ***@gmail.com');

        case validatePhoneNumber(phoneNumber):
            throw createResponseError('queryError', 'Wrong phone number format is +380xxxxxxx');

        case validatePassword(password):
            throw createResponseError('queryError', 'Wrong password minimal length 3');

        default:
            Object.keys(user).forEach(key => {
                user[key] = userData[key];
            });
            return user;

    }
}

const fighterFildsValidations = (fighterData, fighterSchema, services) => {
    const {name, power = 0, defense = 1} = fighterData;
    let fighter = {...fighterSchema};
    delete fighter.id;

    switch (false) {
        case checkFighterData(fighterData, fighter):
            throw createResponseError('queryError', 'All fields requerid');
        case !services.search({name}):
            throw createResponseError('queryError', 'Name is already registered');
        case validateName(name):
            throw createResponseError('queryError', 'Wrong Name only letters and length more then 0');
        case validPowerFild(power):
            throw createResponseError('queryError', 'Wrong Power only digits and more then 0 and less 100');
        case validDefenseField(defense):
            throw createResponseError('queryError', 'Wrong Defense only digits  more then 0');
        default:
            Object.keys(fighter).forEach(key => {
                if (key !== 'health') {
                    fighter[key] = fighterData[key];
                }

            });
            return fighter;
    }

}
const validPowerFild = power => {
    const maxPower = 100;
    return !isNaN(parseInt(power)) && power > 0 && power <= maxPower;
}

const validDefenseField = defense => {
    return !isNaN(parseInt(defense)) && defense > 0;
}

const checkReqData = (data, schema) => {
    const result = Object.keys(schema).every(key => data.hasOwnProperty(key));

    return result;
}

const checkFighterData = (data, schema) => {
    const corect = 3;
    let result = 0;

    Object.keys(schema).forEach(key => {
        if (data.hasOwnProperty(key)) {
            result++;
        }
    });

    return corect === result;
}

const createResponseError = (name, message) => {
    const error = new Error(message);
    error.name = name;
    return error;
}

const validatePassword = password => {
    const minLength = 2;

    return password.trim().length > minLength;
}

const validateEmail = email => {
    const regExp = /^((\.)?\w){1,63}@gmail\.com$/;

    return regExp.test(email);
}

const validatePhoneNumber = (phoneNumber) => {
    const regExp = /^\+380\d{9}$/;

    return regExp.test(phoneNumber);
}

const validateName = name => {
    return isNaN(parseInt(name)) && name.trim().length > 0;
}

exports.usersFildFalidations = usersFildFalidations;
exports.fighterFildsValidations = fighterFildsValidations;
